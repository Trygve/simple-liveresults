<!DOCTYPE html>
<html>

<head>
  <meta name="viewport" content="width=device-width, initial-scale=1" http-equiv="Content-Type" />
  <meta name="theme-color" content="#1a237e">
  <title>Liveresultater</title>

  <link rel="icon" type="image/png" href="./common/kok-144x144.png">
  <link rel="stylesheet" href="common/main.css">
</head>

<body class="theme-auto">
<header class="shadow-8">
  <div class='header-column'>
      <div>
        <img src="./common/kok-144x144.png" width="30" height="30" class="d-inline-block align-top" alt="" loading="lazy">
      </div>
      <div id="header-brand">
      </div>
  </div>
  <div class='header-column'>
      <div class="status-div" id="lastUpdate">status</div>
      <div class="status-div" id="lastMessage">status</div>
      <div class="theme-select">
      Farger:
        <select id="theme">
          <option value="auto">Automatisk</option>
          <option value="light">Lyst</option>
          <option value="dark">Mørkt</option>
        </select>
      </div>
  </div>
</header>

  <ul class="tab-list" id="classes-tabs" role="tablist" style="margin-bottom: 1rem; margin-top: 1rem;">
    <li class="tab" role="presentation">
      <a class="tab-button active" id="alle-tab" data-toggle="tab" href="#tab-alle" onclick="changeTab('alle')" role="tab" aria-controls="alle" aria-selected="true">Alle klasser</a>
    </li>

  </ul>

  <div class="container alert alert-warning hiddendiv" role="alert" id="ConnectionAlert">
    Du har mistet kontakten med serveren. Last inn sida på nytt får å koble til igjen <button type="button" class="btn btn-warning" onclick="location.reload()">Last inn på nytt</button>
  </div>

  <div class="tab-container" id="classes-tab-content">
    <div class="tab-content active" id="alle" role="tabpanel" aria-labelledby="alle">
      <div class="all-result" id="alle-cont"></div>
    </div>
  </div>

  <footer><p>Denne sida er fri programmvare under <a href="http://www.gnu.org/licenses/agpl-3.0.html">AGPL 3.0</a>. Kildekoden er tilgjengelig <a href="https://gitlab.com/Trygve/simple-liveresults">her</a>. </p></footer>
  <script src="ui.js"></script>
  <script src="liveresults.js"></script>
</body>
</html>
