// @license magnet:?xt=urn:btih:0b31508aeb0634b347b8270c7bee4d411b5d4109&dn=agpl-3.0.txt AGPL-v3
function changeTab(selected){
	let selectElement = document.getElementById(selected);
	if (!selectElement.classList.contains('active')) {
		let tabContList = document.getElementsByClassName('tab-content active');
		let tabBtnList = document.getElementsByClassName('tab-button active');
		let selectTabBtn = document.getElementById(`${selected}-tab`);

		if (tabContList[0]) {
			tabContList[0].classList.remove('active');
			tabBtnList[0].classList.remove('active');
		}
		selectElement.classList.add('active');
		selectTabBtn.classList.add('active');
};		
};

//theme
function applyTheme(theme) {
    document.body.classList.remove("theme-auto", "theme-light", "theme-dark");
    document.body.classList.add(`theme-${theme}`);
}
 
document.addEventListener("DOMContentLoaded", () => {
   document.querySelector("#theme").addEventListener("change", function() {
        applyTheme(this.value);
   });
});
 
document.addEventListener("DOMContentLoaded", () => {
  const savedTheme = localStorage.getItem("theme") || "auto";
 
  applyTheme(savedTheme);
 
  for (const optionElement of document.querySelectorAll("#theme option")) {
      optionElement.selected = savedTheme === optionElement.value;
  }
 
  document.querySelector("#theme").addEventListener("change", function () {
      localStorage.setItem("theme", this.value);
      applyTheme(this.value);
  });
});
// @license-end
