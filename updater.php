<?php
set_time_limit( 0 );
ini_set('auto_detect_line_endings', 1);
ini_set('mysql.connect_timeout','7200');
ini_set('max_execution_time', '0');

date_default_timezone_set( 'Europe/Oslo' );
ob_end_clean();
gc_enable();

header('Content-Type: text/event-stream');
header('Cache-Control: no-cache');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Methods: GET');
header('Access-Control-Expose-Headers: X-Events');  

include 'lastSavedChange.php';

$resultFile = "resultater/Resultater.xml";
//load variable from last check
$lastChange = filemtime($resultFile);
$time = date('r');

if ($lastSavedChange == NULL){
    $lastSavedChange = $lastChange;
}
//check if file has been updated
if ($lastSavedChange<$lastChange){
    //update deteceted
    $ischanged = "yes :)";
    $var_str = var_export($lastChange, true);
    $var = "<?php\n\n\$lastSavedChange = $var_str;\n\n?>";
    file_put_contents('lastSavedChange.php', $var, LOCK_EX);
    $objXmlDocument = simplexml_load_file("resultater/Resultater.xml");
 
    //xml to json
    if ($objXmlDocument === FALSE) {
        foreach(libxml_get_errors() as $error) {
            //error :(
        }
        exit;
    }
    $objJsonDocument = json_encode($objXmlDocument);
    $arrOutput = json_decode($objJsonDocument, TRUE);

    //csv format: change boolian , last time the xml was changed, current server time, json from xml
    $response = "1,". $lastChange. "," . time() . "," . $objJsonDocument;
    echo "data: $response\n\n";
}
elseif ($lastSavedChange==NULL){
    //file does not exist
}
else {
    //no update deteceted
    $response = "0,". $lastChange. "," . time() . "," ;
    echo "data: $response\n\n";
}
flush();
?>
